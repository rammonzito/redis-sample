using System;
using StackExchange.Redis;

namespace RedisConnection
{
    public class Connection
    {
        readonly static ConnectionMultiplexer muxer = ConnectionMultiplexer.Connect("localhost:6379");
        readonly IDatabase conn = muxer.GetDatabase();
        public void connect()
        {
            string page = "about";
            HashIncrement(page, "acessos");
            var value = GetPageAccess(page);
            Console.WriteLine(value);
        }

        private RedisValue GetPageAccess(string page)
        {
            return conn.HashGet(page, "acessos");
        }

        private void HashIncrement(string key, string hasField)
        {
            conn.HashIncrement(key, hasField);
        }
    }
}